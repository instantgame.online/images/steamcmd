FROM bitnami/minideb:latest

# Install required dependencies
RUN apt-get update && apt-get install lib32gcc1 ca-certificates -yq

# Create Steam user
RUN useradd -m steam

# Download SteamCMD
ADD https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz /tmp

RUN chown steam /tmp/steamcmd_linux.tar.gz && mkdir /opt/steamcmd; chown steam:steam /opt/steamcmd;

USER steam

# Unpack SteamCMD into /usr/local/bin
RUN cd /opt/steamcmd; tar xf /tmp/steamcmd_linux.tar.gz; rm /tmp/steamcmd_linux.tar.gz; mv /opt/steamcmd/steamcmd.sh /opt/steamcmd/steamcmd

ENV PATH="/opt/steamcmd:${PATH}"

RUN steamcmd +quit

ENTRYPOINT ["/opt/steamcmd/steamcmd"]
